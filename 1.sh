#!/bin/bash

arq1=$(wc -l < "$1")
arq2=$(wc -l < "$2")
arq3=$(wc -l < "$3")
arq4=$(wc -l < "$4")

maior=$arq1
max=$1


if [ $arq2 -gt $maior 2> /dev/null ]; then
 maior=$arq2
 max=$2
fi


if [ $arq3 -gt $maior 2> /dev/null ]; then
 maior=$arq3
 max=$3
fi

if [ $arq4 -gt $maior 2> /dev/null ]; then
 maior=$arq4
 max=$4
fi

echo "$(cat $max)"


