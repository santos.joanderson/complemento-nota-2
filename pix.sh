#!/bin/bash

if [ -e /tmp/$1 ]; then

echo "O arquivo existe em /tmp/pixfeito.txt"

elif [ -e /home/$1 ]; then
echo "O arquivo existe em /home/ifpb"

elif [ -e /proc/$1 ]; then 
echo "O arquivo existe em /proc/pixfeito.txt"

elif [ -e /var/log/$1 ]; then
echo "O arquivo existe em /var/log/pixfeito.txt"

else
echo "O arquivo não existe"

fi
